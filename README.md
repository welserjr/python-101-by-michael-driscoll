# Python 101 by Michael Driscoll

## Part I - Learning the Basics

* Chapter 1 - IDLE Programming
* Chapter 2 - Strings
* Chapter 3 - Lists, Tuples and Dicts
* Chapter 4 - Conditionals
* Chapter 5 - Loops
* Chapter 6 - Comprehensions
* Chapter 7 - Exceptions
* Chapter 8 - Working with Files
* Chapter 9 - Importing
* Chapter 10 - Functions
* Chapter 11 - Classes

## Part II - Learning from the Library

* Chapter 12 - Introspection
* Chapter 13 - Csv
* Chapter 14 - Config Parser
* Chapter 15 - Logging
* Chapter 16 - The os Module
* Chapter 17 - The email / smtplib Module
* Chapter 18 - The sqlite Module
* Chapter 19 - The subprocess Module
* Chapter 20 - The sys Module
* Chapter 21 - Threading
* Chapter 22 - Time
* Chapter 23 - Xml

## Part III - Intermediate Odds and Ends

* Chapter 24 - The Python Debugger
* Chapter 25 - Decorators
* Chapter 26 - Lambda
* Chapter 27 - Profiling
* Chapter 28 - Testing

## Part IV - Tips, Tricks and Tutorials

* Chapter 29 - Installing Modules
* Chapter 30 - Config Obj
* Chapter 31 - Parsing XML with lxml
* Chapter 32 - Code Analysis
* Chapter 33 - Requests
* Chapter 34 - SQLAlchemy
* Chapter 35 - Virtualenv

## Part V - Packaging and Distribution

* Chapter 36 - Creating Modules and Packages
* Chapter 37 - Add your code to pypi
* Chapter 38 - Python eggs
* Chapter 39 - Python wheels
* Chapter 40 - Py2exe
* Chapter 41 - bbfreeze
* Chapter 42 - cxFreeze
* Chapter 43 - PyInstaller
* Chapter 44 - Creating an installer